# Array-Handler
## Programmed by JTKreates

## About

After learning more about sorting and searching arrays in my AP CS class,
I decided to create a class that has everything I learned for easier experimentation.
This is just for my learning expirence and in no way meant for any real use. 
But I thought it might be able to help others as well as help add some content to my GitHub account. 

## Building

Run the following commands

```javac ArrayHandler.java```

```javac ArrayHandlerClient.java```

## Running

If you built from source run

```java ArrayHandlerClient```

If you downloaded the jar file run

```java -jar ArrayHandlerClient.jar```

## Other

I know this potentially has some issues with it, feel free to post them in the issues tab on github. 
Just a side note however, this is not a main focus of mine, so there is no gurantee i'll fix anything or anything within a reasonable amount of time.