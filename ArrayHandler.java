import java.util.Random;

public class ArrayHandler
{
    public int[] fillRandom()
    {
        int[] array = new int[10];

        Random rand = new Random();

        for(int i = 0; i < array.length; i++)
        {
            array[i] = rand.nextInt(9999) + 1;
        }

        return array;
    }

    public int sequentialSearch(int[] array, int key)
    {
        for(int i = 0; i < array.length; i++)
        {
            if(array[i] == key)
                return i;
        }

        return -1;
    }

    public int binarySearch(int[] array, int key)
    {
        int low = 0;
        int high = array.length - 1;
        while(low <= high)
        {
            int mid = (low + high) / 2;

            if(array[mid] == key)
                return mid;
            if(array[mid] < key)
                low = mid - 1;
            if(array[mid] > key)
                low = mid + 1;
        }

        return -1;
    }

    public int jumpSearch(int[] array, int key)
    {
        int len = array.length;

        int step = (int)Math.floor(Math.sqrt(len));

        int prev = 0;
        while(array[Math.min(step, len) - 1] < key)
        {
            prev = step;
            step += (int)Math.floor(Math.sqrt(len));
            if(prev >= len)
                return -1;
        }

        while(array[prev] < key)
        {
            prev++;

            if(prev == Math.min(step, len))
                return -1;
        }

        if(array[prev] == key)
            return prev;

        return -1;
    }

    public int interpolationSearch(int[] array, int key)
    {
        int low = 0, high = array.length - 1;

        while(low <= high && key >= array[low] && key <= array[high])
        {
            int position = low + (((high - low) / (array[high] - array[low])) * (key - array[low]));

            if(array[position] == key)
                return position;

            if(array[position] < key)
                low = position + 1;

            if(array[position] > key)
                high = position - 1;
        }

        return -1;
    }

    public int[] selectionSort(int[] array)
    {
        for(int i = 0; i < array.length - 1; i++)
        {
            int mIndex = i;
            for(int j = i + 1; j < array.length; j++)
            {
                if(array[j] < array[i])
                    mIndex = j;
            }

            int temp = array[mIndex];
            array[mIndex] = array[i];
            array[i] = temp;
        }

        return array;
    }

    public int[] bubbleSort(int[] array)
    {
        for(int i = 0; i < array.length - 1; i++)
        {
            for(int j = 0; j < array.length - i - 1; j++)
            {
                if(array[j] > array[j + 1])
                {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }

        return array;
    }

    public int[] insertionSort(int[] array)
    {
        for(int i = 1; i < array.length; ++i)
        {
            int k = array[i];
            int j = i - 1;

            while(j >= 0 && array[j] > k)
            {
                array[j + 1] = array[j];
                j = j - 1;
            }

            array[j + 1] = k;
        }

        return array;
    }

    public int[] mergeSort(int[] array, int lo, int hi)
    {
        int low = lo;
        int high = hi;
        if(low >= high)
        {
            return array;
        }

        int mid = (low + high) / 2;
        mergeSort(array, low, mid);
        mergeSort(array, mid + 1, high);

        int eLow = mid;
        int sHigh = mid + 1;
        while((lo <= eLow) && (sHigh <= high))
        {
            if(array[low] < array[sHigh])
                low++;
            else
            {
                int temp = array[sHigh];
                for(int k = sHigh - 1; k >= low; k--)
                {
                    array[k + 1] = array[k];
                }

                array[low] = temp;
                low++;
                eLow++;
                sHigh++;
            }
        }

        return array;
    }
}
