import java.util.Scanner;

public class ArrayHandlerClient
{
    public static void main(String[] args)
    {
        ArrayHandler ah = new ArrayHandler();
        Scanner scan = new Scanner(System.in);

        int[] a1 = ah.fillRandom();
        int[] a2 = ah.fillRandom();
        int[] a3 = ah.fillRandom();
        int[] a4 = ah.fillRandom();

        System.out.println("First Array Being Sorted With Selection Sort");
        for(int i = 0; i < a1.length; i++)
        {
            System.out.print(a1[i] + "\t");
        }
        int[] a1Sorted = ah.selectionSort(a1);
        System.out.println();
        for(int i = 0; i < a1Sorted.length; i++)
        {
            System.out.print(a1Sorted[i] + "\t");
        }

        System.out.print("\n\n");

        System.out.println("Second Array Being Sorted With Insertion Sort");
        for(int i = 0; i < a2.length; i++)
        {
            System.out.print(a2[i] + "\t");
        }
        int[] a2Sorted = ah.insertionSort(a2);
        System.out.println();
        for(int i = 0; i < a2Sorted.length; i++)
        {
            System.out.print(a2Sorted[i] + "\t");
        }

        System.out.print("\n\n");

        System.out.println("Third Array Being Sorted With Bubble Sort");
        for(int i = 0; i < a3.length; i++)
        {
            System.out.print(a3[i] + "\t");
        }
        int[] a3Sorted = ah.bubbleSort(a3);
        System.out.println();
        for(int i = 0; i < a3Sorted.length; i++)
        {
            System.out.print(a3Sorted[i] + "\t");
        }

        System.out.print("\n\n");

        System.out.println("Fourth Array Being Sorted With Merge Sort");
        for(int i = 0; i < a4.length; i++)
        {
            System.out.print(a4[i] + "\t");
        }
        int[] a4Sorted = ah.mergeSort(a4, 0, 9);
        System.out.println();
        for(int i = 0; i < a4Sorted.length; i++)
        {
            System.out.print(a4Sorted[i] + "\t");
        }

        System.out.print("\n\n");

        System.out.println("Searching First Array With Sequential Search");
        System.out.println("Enter a number to search for");

        int key1 = scan.nextInt();
        int index1 = ah.sequentialSearch(a1Sorted, key1);
        System.out.println("Result found at index " + index1);

        System.out.print("\n\n");

        System.out.println("Searching Second Array With Binary Search");
        System.out.println("Enter a number to search for");

        int key2 = scan.nextInt();
        int index2 = ah.binarySearch(a2Sorted, key2);
        System.out.println("Result found at index " + index2);

        System.out.print("\n\n");

        System.out.println("Searching Third Array With Jump Search");
        System.out.println("Enter a number to search for");

        int key3 = scan.nextInt();
        int index3 = ah.jumpSearch(a3Sorted, key3);
        System.out.println("Result found at index " + index3);

        System.out.print("\n\n");

        System.out.println("Searching Fourth Array With Interpolation Search");
        System.out.println("Enter a number to search for");

        int key4 = scan.nextInt();
        int index4 = ah.interpolationSearch(a4Sorted, key4);
        System.out.println("Result found at index " + index4);
    }
}